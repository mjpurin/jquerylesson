$(function(){
  //windowオブジェクトをjQueryにしてキャッシュ
  var $window=$(window);
  //navBoxオブジェクトをjQueryにしてキャッシュ
  var $navBox=$('#navBox');
  //#navBox要素の複製をメモリ上に作成
  var $navBoxClone=$navBox.clone();
  //新規に要素をメモリ上に作成
  var $cloneWrapper=$('<div id="cloneWrapper"></div>');
  //ラッパー要素の子要素にクローンを追加(メモリ上)
  $cloneWrapper.append($navBoxClone);
  //#containerの要素にラッパー要素を配置(ここではじめて実際に配置される)
  $('#container').append($cloneWrapper);
  //offset().topでドキュメントの上からの距離を取得、outerHeight()で要素の高さを取得
  var changePoint=$navBox.offset().top+$navBox.outerHeight();
  //スクロール処理をプラグインを使って200msごとに1回の処理にする
  $window.on('scroll',$.throttle(200,function(){
    //スクロール位置がchangePointより大きくなったら
    if($window.scrollTop()>changePoint){
      //クラスを付与
      $cloneWrapper.addClass('sticky');
    }else{
      //クラスを削除
      $cloneWrapper.removeClass('sticky');
    }
  }));
});
